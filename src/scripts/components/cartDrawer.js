import { component } from 'picoapp'
import { getSizedImageUrl, imageSize } from '@/lib/images.js'
import { formatMoney } from '@/lib/currency.js'
import app from '@/app.js'
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock'

const X = `<svg width="44px" height="44px" viewBox="0 0 44 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
        <g id="Shop---Mobile" transform="translate(-374.000000, -259.000000)" stroke="#000000" stroke-width="4">
            <g id="Group" transform="translate(376.000000, 243.000000)">
                <g id="plus-copy" transform="translate(0.000000, 18.000000)">
                    <path d="M20,0.25 L20,40" id="Line-5"></path>
                    <path d="M20,0 L20,40" id="Line-5" transform="translate(20.000000, 20.000000) rotate(-90.000000) translate(-20.000000, -20.000000) "></path>
                </g>
            </g>
        </g>
    </g>
</svg>`

function createItem ({
  variant_id: id,
  product_title: title,
  line_price: price,
  variant_title: color,
  image,
  url,
  quantity,
  ...item
}) {
  const img = image ? getSizedImageUrl(
    image.replace('.' + imageSize(image), ''), '200x' // TODO hacky af
  ) : 'https://source.unsplash.com/R9OS29xJb-8/2000x1333'

  return `
<div class='cart-drawer__item overflow--hidden' data-component='cartDrawerItem' data-id=${id}>
  <div class='f'>
    <div class=''>
      <div class='pr-8'>
        <a href='${url}'>
          <img src='${img}' />
        </a>
      </div>
    </div>
    <div class='__content fa rel fs-20 ls-18 f fdc'>
      <div class='fa'>
        <a href='${url}' class='semi mb-7 pr-8'>${title}</a>

        <div class='mb-7'>
          ${color ? `<div class='semi c-gray'>${color.split(':')[0]}</div>` : ``}
        </div>
      </div>

      <div class='f jcb fw'>
        <div class='f aic'>
          <div class='cart-quantity js-remove-single px05'>-</div>
          <div class='js-single-quantity'>${quantity}</div>
          <div class='cart-quantity js-add-single px05'>+</div>
        </div>
        <div class='c-gray'>${formatMoney(price)}</div>
      </div>


      <button class='button--reset js-remove-item abs right'>${X}</button>
    </div>
  </div>
</div>
`
}

function renderItems (items) {
  return items.length > 0 ? (
    items.reduce((markup, item) => {
      markup += createItem(item)
      return markup
    }, '')
  ) : (
    `<div class='pv1'><p class='pt-10 pb-10 i ac'>Your bag is empty</p></div>`
  )
}

export default component((node, ctx) => {
  const overlay = node.querySelector('.js-overlay')
  // const closeButton = node.querySelector('.js-close')
  // const subtotal = node.querySelector('.js-subtotal')
  const itemsRoot = node.querySelector('.js-items')
  const loading = itemsRoot.innerHTML

  const render = (cart) => {
    itemsRoot.innerHTML = renderItems(cart.items)
    // subtotal.innerHTML = formatMoney(cart.total_price)
  }

  const targetElement = document.querySelector('.cart-drawer__items')

  const open = (cart) => {
    node.classList.add('is-active')
    itemsRoot.innerHTML = loading
    setTimeout(() => {
      node.classList.add('is-visible')
      disableBodyScroll(targetElement)
      setTimeout(render(cart), 10)
      app.mount()
    }, 50)
  }

  const close = () => {
    node.classList.remove('is-visible')
    enableBodyScroll(targetElement)
    setTimeout(() => {
      node.classList.remove('is-active')
      app.hydrate({cartOpen: false})
    }, 400)
  }

  const closeButton = node.querySelector('.js-close')

  const checkoutButton = node.querySelector('.js-checkout')



  closeButton.addEventListener('click', close)

  render(ctx.getState().cart)

  overlay.addEventListener('click', close)
  // closeButton.addEventListener('click', close)

  ctx.on('cart:toggle', ({ cart, cartOpen }) => {
    cartOpen && open(cart)
  })
  ctx.on('cart:updated', () => {
    render(ctx.getState().cart)
    app.mount()
  })
})
