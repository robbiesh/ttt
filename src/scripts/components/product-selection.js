import { component } from 'picoapp'
import { addVariant } from '@/lib/cart.js'
import radio from '@/lib/radio.js'
import options from '@/lib/options.js'
import getProductJson from '@/lib/getProductJson.js'

export default component(( node ) => {
  const opts = options(node)

  // cache
  getProductJson()

  opts.onUpdate(state => {
    getProductJson().then(json => {
      const variant = json.variants.filter(v => v.id == state.id)[0]
    })
  })

  const desc = node.querySelector('.js-desc')

  const desc_titles = node.querySelectorAll('.js-title')

  for (let i = 0; i < desc_titles.length; i++) {
    desc_titles[i].addEventListener('click', e => {
      var d = desc_titles[i].dataset.desc
      desc.classList.remove('desc-1')
      desc.classList.remove('desc-2')
      desc.classList.add('desc-' + d)
    })
  }

})
