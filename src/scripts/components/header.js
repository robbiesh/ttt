import { component } from 'picoapp'

export default component((node, ctx) => {
  const cartCount = node.querySelector('.js-cart-count')
  const cartToggles = node.querySelectorAll('.js-cart-drawer-toggle')

  for (let i = 0; i < cartToggles.length; i++) {
    cartToggles[i].addEventListener('click', e => {
      e.preventDefault()

      ctx.emit('cart:toggle', state => {
        return {
          cartOpen: !state.cartOpen
        }
      })
    })
  }

  ctx.on('cart:updated', state => {
    cartCount.innerHTML = state.cart.item_count
  })

  cartCount.innerHTML = ctx.getState().cart.item_count

  const body = document.getElementsByTagName("body")[0]
  const hb = node.querySelector('.js-hamburger')

  hb.addEventListener('click', (e) => {
    e.preventDefault()

    body.classList.toggle('mm--hidden')
  })

  var prevScrollpos = window.pageYOffset;
  var header = node;
  var header_hidden = false;

  window.onscroll = function() {

  	var currentScrollPos = window.pageYOffset;

  	if (currentScrollPos > 100) {
      header.classList.add('site-header--open');
      body.classList.add('cart--open');
  	} else {
      header.classList.remove('site-header--open');
      body.classList.remove('cart--open');
    }

    prevScrollpos = currentScrollPos;
  }

  // var menu_items = node.querySelectorAll('.js-menu-item')
  //
  // for (let i = 0; i < menu_items.length; i++) {
  //   menu_items[i].addEventListener('click', e => {
  //     e.preventDefault()
  //
  //     for (let j = 0; j < menu_items.length; j++) {
  //       menu_items[j].classList.remove('active')
  //     }
  //
  //     menu_items[i].classList.add('active')
  //   })
  // }


})
