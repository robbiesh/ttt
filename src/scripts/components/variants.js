import { component } from 'picoapp'
import isotope from 'isotope-layout'

export default component((node, ctx) => {

  const toggle = node.querySelector('.js-toggle')

  toggle.addEventListener('click', function() {
    toggle.classList.toggle('open')
  });

  const product_filters = node.querySelectorAll('.js-filter-product')

  const products = node.querySelectorAll('.js-product')

  for (let product_filter of product_filters) {
    product_filter.addEventListener('click', e => {

      const items = node.querySelectorAll('.' + product_filter.dataset.filter)

      if (product_filter.classList.contains('active')) {

        // do the products

        for (let product of products) {
          product.classList.add('active')
        }

        // do the filter

        product_filter.classList.remove('active')

      }

      else {

        // do the products

        for (let product of products) {
          product.classList.remove('active')
        }

        for (let item of items) {
          item.classList.add('active')
        }

        // do the filter

        for (let inner_product_filter of product_filters) {
          inner_product_filter.classList.remove('active')
        }

        product_filter.classList.add('active')
      }
    })
  }

  const color_filters = node.querySelectorAll('.js-filter-color')

  const variants = node.querySelectorAll('.js-variant')

  for (let color_filter of color_filters) {
    color_filter.addEventListener('click', e => {

      if (color_filter.classList.contains('active')) {

        // do the products

        for (let variant of variants) {
          variant.classList.add('active')
        }

        // do the filter
        color_filter.classList.remove('active')
      }

      else {

        for (let variant of variants) {
          variant.classList.remove('active')
        }

        const items = node.querySelectorAll('.' + color_filter.dataset.filter)

        for (let item of items) {
          item.classList.add('active')
        }

        for (let inner_color_filter of color_filters) {
          inner_color_filter.classList.remove('active')
        }

        color_filter.classList.add('active')

      }

    })
  }

})
