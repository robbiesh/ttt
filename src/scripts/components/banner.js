import { component } from 'picoapp'
import Flickity from 'flickity'
import FlickityImagesLoaded from 'flickity-imagesloaded'
import FlickityFade from 'flickity-fade'

export default component((node, ctx) => {

  var slider = new Flickity( '.js-banner', {
    contain: true,
    pageDots: true,
    wrapAround: true,
    imagesLoaded: true,
    fade: true,
    pageDots: true,
    autoPlay: false,
    pauseAutoPlayOnHover: false
  });

})
