import { component } from 'picoapp'

export default component((node, ctx) => {

  const popup_toggles = node.querySelectorAll('.footer-popup-toggle')

  for (let popup_toggle of popup_toggles) {
    popup_toggle.addEventListener('click', function(e) {
      e.preventDefault()
      popup_toggle.classList.toggle('open')
    })
  }

  const popups = node.querySelectorAll('.footer-popup')

  for (let popup of popups) {
    popup.addEventListener('click', function(e) {
      e.preventDefault()
      node.querySelector('.footer-popup-toggle[data-popup="' + popup.dataset.popup + '"]').classList.toggle('open')
    })
  }

})
