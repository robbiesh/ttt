import { component } from 'picoapp'
import wait from 'w2t'
import noodle from 'noodle'
import Poppy from 'poppy'
import options from '../slater/product-options.js'
import variants from '../slater/product-controller.js'
import tink from 'tinkerbell'

import Flickity from 'flickity'
import FlickityImagesLoaded from 'flickity-imagesloaded'
import FlickityFade from 'flickity-fade'

import '@/lib/jquery-global.js'
import elevateZoom from '@zeitiger/elevatezoom'

function ease (t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t + b
  return -c / 2 * ((--t) * (t - 2) - 1) + b
}

export default component((node, actions) => {

  // var slider = new Flickity( '.js-product-slider', {
  //   contain: true,
  //   pageDots: false,
  //   wrapAround: true,
  //   imagesLoaded: true,
  //   fade: false,
  //   pageDots: false
  // });
  //
  // slider.on( 'select', function( index ) {
  //
  // });

  // console.log(node);
  // console.log(actions);

  const form = node.querySelector('.js-add-to-cart-form')

  const submit = form.querySelector('button[type="submit"]')
  const submitCta = submit.querySelector('.js-cta')
  const submitCtaText = submitCta.getAttribute('data-cta')
  const availability = node.querySelector('.js-availability')
  const price = node.querySelector('.js-price')
  const comparePrice = node.querySelector('.js-compare-price')
  const inventory = JSON.parse(document.querySelector('.js-inventory').innerHTML)
  // const tooltipToggle = node.querySelector('.js-tooltip')

  const submitCtaButton = form.querySelector('.js-cta-button')

  // const navDots = [].slice.call(node.querySelector('.js-slider-dots').children)
  const navButtons = node.querySelectorAll('.js-slider button')

  // const sliderOuter = node.querySelector('.js-product-slider')

  // const slider = noodle(sliderOuter, {
  //   a11y: false,
  //   setHeight: false
  // }, 'slider')

  const toggle = document.querySelector('.js-accordion-toggle')

  // if (affirm) {
  //   affirm.ui.refresh()
  // }

  // const navOuter = node.querySelector('.js-product-slider-nav')
  // const navSliderOuter = node.querySelector('.js-slider')
  const controls = node.getElementsByTagName('nav')[0]
  // const total = navSliderOuter.children.length
  // const [ prev, next ] = controls.getElementsByTagName('button')
  const prev = node.querySelector('.js-prev')
  const next = node.querySelector('.js-next')
  // const currInd = controls.querySelector('.js-curr')
  // const totalInd = controls.querySelector('.js-total')
  const counter = node.querySelector('.js-counter-quantity')

  // const minusResize = node.querySelector('.js-minus-size')
  // minusResize.style.height = '2px'
  // setTimeout(() => {
  //   minusResize.style.height = '2px'
  // }, 100)

  // const sel = document.getElementById('selectsize');
  //
  // const navSlider = ''
  // const navSlider = noodle(navSliderOuter, {
  //   a11y: false,
  //   setHeight: false
  // }, 'nav')

  // navSliderOuter.tabIndex = -1;

  // const navItems = node.querySelectorAll('.js-slider-item');
  //
  // [].forEach.call(navItems, function(div) {
  //   // do whatever
  //   div.tabIndex = -1;
  // });
  //
  // const prodItems = node.querySelectorAll('.js-prod-item');
  //
  // [].forEach.call(prodItems, function(div) {
  //   // do whatever
  //   div.tabIndex = -1;
  // });


  // function setNavSliderControls (i) {
  //   // currInd.innerHTML = i + 1
  //
  //   if (i === 0) {
  //     prev.disabled = true
  //     next.disabled = false
  //   } else if (i === total - 1) {
  //     next.disabled = true
  //     prev.disabled = false
  //   } else {
  //     prev.disabled = false
  //     next.disabled = false
  //   }
  // }

  // function setActiveThumb (index) {
  //
  //   for (let i = 0; i < total; i++) {
  //     navButtons[i].parentNode.classList.remove('is-selected')
  //     // navDots[i].classList.remove('is-selected')
  //   }
  //
  //   navButtons[index].parentNode.classList.add('is-selected')
  //   // navDots[index].classList.add('is-selected')
  // }

  const meta = JSON.parse(document.querySelector('.js-meta').innerHTML)

  const variant_ids = meta.variants

  // set the thumbnails

  // for (let variant of variant_ids) {
  //   var thumbnails = node.querySelectorAll('.js-thumbnail-' + variant.id)
  //   var wrapper = document.createElement('div');
  //   wrapper.setAttribute('class', 'js-thumbnails-' + variant.id);
  //
  //   for (let thumbnail of thumbnails) {
  //     thumbnail.parentNode.insertBefore(wrapper, thumbnail);
  //     wrapper.appendChild(thumbnail);
  //   }
  // }

  // for (let variant of variant_ids) {
  //   var thumbnails = node.querySelectorAll('.js-image-' + variant.id)
  //   var wrapper = document.createElement('div');
  //   wrapper.setAttribute('class', 'js-slider-' + variant.id);
  //
  //   for (let thumbnail of thumbnails) {
  //     thumbnail.parentNode.insertBefore(wrapper, thumbnail);
  //     wrapper.appendChild(thumbnail);
  //   }
  // }

  // setNavSliderControls(0)
  // setActiveThumb(slider.index)

  // navSlider.on('select', index => {
  //   slider.select(index)
  //   // setNavSliderControls(index)
  //   setActiveThumb(slider.index)
  // })
  //
  // slider.on('select', index => {
  //   navSlider.select(index)
  //   // setNavSliderControls(index)
  //   setActiveThumb(index)
  //
  // })

  // for (let i = 0; i < navButtons.length; i++) {
  //   navButtons[i].addEventListener('click', e => {
  //     slider.select(i)
  //   })
  // }
  //
  // function getImageIndex (id) {
  //   const images = sliderOuter.getElementsByTagName(`img`)
  //
  //   // console.log(images.length)
  //
  //   for (let i = 0; i < images.length; i++) {
  //     // console.log(id);
  //     // console.log(images[i].src);
  //     if (images[i].src.indexOf(id) > -1) return i
  //   }
  // }

  function getInventory (id) {
    return inventory.variants.filter(v => v.id === id)[0]
  }

  function buildCounter (pmax) {
    const decrease = node.querySelector('.js-counter-remove')
    const increase = node.querySelector('.js-counter-add')
    const quantity = node.querySelector('.js-counter-quantity')

    var min = ''
    var max = ''

    if (pmax) {
      max = pmax
    } else {
      max = parseInt(quantity.attributes.max.value)
    }

    let count = parseInt(quantity.value)

    const set = (i) => {
      count = Math.max(min, Math.min(i, max || 10000))
      quantity.value = count
    }

    if (quantity.value > max) {
      set(max)
    }

    decrease.addEventListener('click', e => {
      e.preventDefault()
      set(--count)
    })

    increase.addEventListener('click', e => {
      e.preventDefault()
      set(++count)
    })
  }

  const pdp_sliders = node.querySelectorAll('.pdp-slider')
  const pdp_thumbnails = node.querySelectorAll('.pdp-thumbnails')

  const pdp_flickities = []

  const pdp_thumbnail_flickities = []

  for (let pdp_slider of pdp_sliders) {

    pdp_flickities[pdp_slider.dataset.id] = new Flickity( pdp_slider, {
      contain: true,
      pageDots: false,
      wrapAround: true,
      imagesLoaded: true,
      fade: false,
      pageDots: false,
      prevNextButtons: false
    });

    pdp_flickities[pdp_slider.dataset.id].on( 'change', function(index) {

      const thumbnails = node.querySelector('.js-thumbnails-' + pdp_slider.dataset.id)
      const thumbnail_items = thumbnails.querySelectorAll('.js-thumbnail')

      for (let thumbnail_item of thumbnail_items) {
        thumbnail_item.classList.remove('active')
      }

      const img = thumbnails.querySelector('[data-index="' + index + '"]')
      img.classList.add('active')

      // $('.zoomContainer').remove();
      // $('.zoomable').removeData('elevateZoom');
      // $('.zoomable').removeData('zoomImage');

      // $(pdp_slider).find('.is-selected .zoomable').elevateZoom({
      //   zoomType: 'lens',
      //   lensShape: 'round',
      //   lensSize: 200
      // });

    });

  }

  for (let pdp_thumbnail of pdp_thumbnails) {

    const pdp_thumbnail_items = pdp_thumbnail.querySelectorAll('.js-thumbnail')

    for (let pdp_thumbnail_item of pdp_thumbnail_items) {
      pdp_thumbnail_item.addEventListener('click', e => {
        e.preventDefault()
        pdp_flickities[pdp_thumbnail.dataset.id].select(pdp_thumbnail_item.dataset.index)
      })
    }
  }

  function updateSliders(id) {
    for (let pdp_slider of pdp_sliders) {
      pdp_slider.classList.remove('active')
      
      // $('.zoomContainer').remove();
      // $('.zoomable').removeData('elevateZoom');
      // $('.zoomable').removeData('zoomImage');
    }

    var pdp_slider_active = node.querySelector('.js-slider-' + id)
    pdp_slider_active.classList.add('active')

    // if ($(window).width() >= 750) {
    //   $(pdp_slider_active).find('.is-selected .zoomable').elevateZoom({
    //     zoomType: 'lens',
    //     lensShape: 'round',
    //     lensSize: 200
    //   });
    // }

    pdp_flickities[id].resize()


    for (let pdp_thumbnail of pdp_thumbnails) {
      pdp_thumbnail.classList.remove('active')
    }

    node.querySelector('.js-thumbnails-' + id).classList.add('active')
  }

  // updateSliders(currentVariant.id)

  // $carousels.on( 'select.flickity', function( event ) {
  //   var $target = $( event.currentTarget );
  //   var flkty = $target.data('flickity');
  //   var selectedIndex = flkty.selectedIndex;
  //   $carousels.each( function( i, carousel ) {
  //     var $carousel = $( carousel );
  //     flkty = $carousel.data('flickity');
  //     if ( flkty.selectedIndex != selectedIndex ) {
  //       $carousel.flickity( 'select', selectedIndex );
  //     }
  //   });
  // });

  buildCounter()

  variants().then(vars => {
    options(node, (name, value) => {

      vars.set(name, value)
      const curr = vars.selected

      const id = curr.id
      const inv = getInventory(id)

      updateSliders(id)

      buildCounter(inv.inventory_quantity)

      price.innerHTML = '$' + curr.price / 100

      if (inv.incoming && inv.inventory_policy === 'continue' && inv.inventory_quantity <= 0) {
        submit.disabled = false
        submitCta.innerHTML = submitCtaText

        if (availability) {
          availability.children[0].innerHTML = `Available on ${inv.next_incoming_date}`
          availability.classList.remove('hide')
        }
      } else {
        availability && availability.classList.add('hide')

        if (curr.available) {
          // submit.disabled = false
          // currently always showing "Add to Cart", unless the preorder metafield is checked
          // if (inv.inventory_quantity <= 0) {
          //   submitCta.innerHTML = 'Pre Order'
          // } else {
          // }

          // if (sel !== null) {
          //   if (sel.options[sel.selectedIndex].value == 'select-a-size') {
          //
          //   } else {
          //     submitCta.innerHTML = submitCtaText
          //     // submitCtaButton.classList.remove('bg-dark-gray')
          //     // submitCtaButton.classList.add('bg-black')
          //     submitCtaButton.disabled = false
          //   }
          // } else {
          //   submitCta.innerHTML = submitCtaText
          //   // submitCtaButton.classList.remove('bg-dark-gray')
          //   // submitCtaButton.classList.add('bg-black')
          //   submitCtaButton.disabled = false
          // }

        } else {
          submitCta.innerHTML = 'Sold Out'
          // submitCtaButton.classList.remove('bg-black')
          // submitCtaButton.classList.add('bg-dark-gray')
          submitCtaButton.disabled = true
          // submit.disabled = true
        }
      }
    })


    // if (sel !== null) {
    //   sel.addEventListener('change', e => {
    //     submitCta.innerHTML = submitCtaText
    //     submitCtaButton.classList.remove('bg-dark-gray')
    //     submitCtaButton.classList.add('bg-black')
    //   })
    // }

    // form.addEventListener('submit', e => {
    //   e.preventDefault()
    //
    //   // wait(1000, [
    //   //   actions.addToCart({ id: vars.selected.id, props: { quantity: e.target.elements.quantity.value } })
    //   // ]).then(([ err ]) => {
    //   //   err && console.log(err)
    //   // })
    // })

  })

  const { selectedOrFirstAvailableVariant, product } = JSON.parse(document.querySelector('.js-product-json').innerHTML)
  let currentVariant = product.variants.filter(v => v.id === selectedOrFirstAvailableVariant)[0]

  updateSliders(currentVariant.id)


  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  *
  *   File:   radioGroup.js
  *
  *   Desc:   Radio group widget that implements ARIA Authoring Practices
  */

  /*
  *   @constructor radioGroup
  *
  *
  */
  var RadioGroup = function (domNode) {

    this.domNode   = domNode;

    this.radioButtons = [];

    this.firstRadioButton  = null;
    this.lastRadioButton   = null;

  };

  RadioGroup.prototype.init = function () {

    // initialize pop up menus
    if (!this.domNode.getAttribute('role')) {
      this.domNode.setAttribute('role', 'radiogroup');
    }

    var rbs = this.domNode.querySelectorAll('[role=radio]');

    for (var i = 0; i < rbs.length; i++) {
      var rb = new RadioButton(rbs[i], this);
      rb.init();
      this.radioButtons.push(rb);

      // console.log(rb);

      if (!this.firstRadioButton) {
        this.firstRadioButton = rb;
      }
      this.lastRadioButton = rb;
    }
    this.firstRadioButton.domNode.tabIndex = 0;
  };

  RadioGroup.prototype.setChecked  = function (currentItem) {
    for (var i = 0; i < this.radioButtons.length; i++) {
      var rb = this.radioButtons[i];
      rb.domNode.setAttribute('aria-checked', 'false');
      rb.domNode.tabIndex = -1;
      // rb.domNode.childNodes[1].checked = "";
      rb.domNode.childNodes[1].checked = false;
    }
    currentItem.domNode.setAttribute('aria-checked', 'true');
    currentItem.domNode.tabIndex = 0;
    currentItem.domNode.focus();

    currentItem.domNode.childNodes[1].checked = true;
    currentItem.domNode.childNodes[1].dispatchEvent(new Event('click'));
  };



  RadioGroup.prototype.setCheckedToPreviousItem = function (currentItem) {
    var index;

    if (currentItem === this.firstRadioButton) {
      this.setChecked(this.lastRadioButton);
    }
    else {
      index = this.radioButtons.indexOf(currentItem);
      this.setChecked(this.radioButtons[index - 1]);
    }
  };

  RadioGroup.prototype.setCheckedToNextItem = function (currentItem) {
    var index;

    if (currentItem === this.lastRadioButton) {
      this.setChecked(this.firstRadioButton);
    }
    else {
      index = this.radioButtons.indexOf(currentItem);
      this.setChecked(this.radioButtons[index + 1]);
    }
  };

  /*
  *   This content is licensed according to the W3C Software License at
  *   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
  *
  *   File:   RadioButton.js
  *
  *   Desc:   Radio button widget that implements ARIA Authoring Practices
  */

  /*
  *   @constructor RadioButton
  *
  *
  */
  var RadioButton = function (domNode, groupObj) {

    this.domNode = domNode;
    this.radioGroup = groupObj;

    this.keyCode = Object.freeze({
      'RETURN': 13,
      'SPACE': 32,
      'END': 35,
      'HOME': 36,
      'LEFT': 37,
      'UP': 38,
      'RIGHT': 39,
      'DOWN': 40
    });
  };

  RadioButton.prototype.init = function () {
    this.domNode.tabIndex = -1;
    this.domNode.setAttribute('aria-checked', 'false');

    this.domNode.addEventListener('keydown',    this.handleKeydown.bind(this));
    this.domNode.addEventListener('click',      this.handleClick.bind(this));
    this.domNode.addEventListener('focus',      this.handleFocus.bind(this));
    this.domNode.addEventListener('blur',       this.handleBlur.bind(this));

  };

  /* EVENT HANDLERS */

  RadioButton.prototype.handleKeydown = function (event) {
    var tgt = event.currentTarget,
      flag = false,
      clickEvent;

    //  console.log("[RadioButton][handleKeydown]: " + event.keyCode + " " + this.radioGroup)

    switch (event.keyCode) {
      case this.keyCode.SPACE:
      case this.keyCode.RETURN:
        this.radioGroup.setChecked(this);
        flag = true;
        break;

      case this.keyCode.UP:
        this.radioGroup.setCheckedToPreviousItem(this);
        flag = true;
        break;

      case this.keyCode.DOWN:
        this.radioGroup.setCheckedToNextItem(this);
        flag = true;
        break;

      case this.keyCode.LEFT:
        this.radioGroup.setCheckedToPreviousItem(this);
        flag = true;
        break;

      case this.keyCode.RIGHT:
        this.radioGroup.setCheckedToNextItem(this);
        flag = true;
        break;

      default:
        break;
    }

    if (flag) {
      event.stopPropagation();
      event.preventDefault();
    }
  };

  RadioButton.prototype.handleClick = function (event) {
    this.radioGroup.setChecked(this);
  };

  RadioButton.prototype.handleFocus = function (event) {
    this.domNode.classList.add('focus');
  };

  RadioButton.prototype.handleBlur = function (event) {
    this.domNode.classList.remove('focus');
  };

  // var rg1 = new RadioGroup(document.getElementById('rg1'));
  // rg1.init();

})
