import { picoapp } from 'picoapp'

import slaterWelcome from '@/components/slater-welcome.js'

import header from '@/components/header.js'
import pdpHero from '@/components/pdpHero.js'
import productSelection from '@/components/product-selection.js'
import cartDrawer from '@/components/cartDrawer.js'
import cartDrawerItem from '@/components/cartDrawerItem.js'
import accountLogin from '@/components/accountLogin.js'
import product from '@/components/product.js'
import productCounter from '@/components/product-counter.js'
import ig from '@/components/ig.js'
import variants from '@/components/variants.js'
import cartItem from '@/components/cartItem.js'
import banner from '@/components/banner.js'
import footer from '@/components/footer.js'

const state = {
  cartOpen: false
}

const components = {
  slaterWelcome,
  accountLogin,
  header,
  productSelection,
  cartDrawer,
  cartDrawerItem,
  product,
  productCounter,
  ig,
  variants,
  pdpHero,
  cartItem,
  banner,
  footer
}

export default picoapp(components, state)
