import '../styles/main.css'
import '@/lib/select.js'
// import lazim from 'lazim'
import app from '@/app.js'
// import barba from '@barba/core';
// import router from '@/router.js'
import { fetchCart } from '@/lib/cart.js'
import '@/lib/jquery-global.js'

window.scrollTo(0, 0);

/**
 * store binding fn
 */
// const images = lazim()

/**
 * bind on page load
 */
// images()

// app.unmount()
// app.mount()

// router.on('after', () => {
//   app.unmount()
//   app.mount()
//
//   $('.zoomContainer').remove();
//   $('.zoomable').removeData('elevateZoom');
//   $('.zoomable').removeData('zoomImage');
//   /**
//    * bind new images
//    */
//   // images()
// })

// barba.init({
//   transitions: [{
//
//     leave({ current, next, trigger }) {
//       // do something with `current.container` for your leave transition
//       // then return a promise or use `this.async()`
//       document.body.classList.add('is-transitioning')
//       setTimeout(() => document.body.classList.remove('is-transitioning'), 1000)
//
//       setTimeout(() => {
//         // const nav = document.querySelector('.js-nav')
//         // nav.classList.remove('nav--open')
//         // nav.classList.add('nav--closed')
//         window.scrollTo(0, 0)
//
//       }, 300)
//
//       $('.zoomContainer').remove();
//       $('.zoomable').removeData('elevateZoom');
//       $('.zoomable').removeData('zoomImage');
//
//     },
//     enter({ current, next, trigger }) {
//
//       setTimeout(() => {
//         // document.title = title
//         // window.history.pushState({}, '', next.url)
//         app.unmount()
//         app.mount()
//
//         // setTimeout(() => {
//         //   aos.init();
//         // }, 1500)
//
//         // console.log(window.location.hash)
//
//         if(window.location.hash) {
//           sscroll(document.getElementById(window.location.hash.substr(1)), {
//             duration: 1500, // ms
//             offset: 0 // positive values scroll further
//           })
//         }
//
//       }, 600)
//
//       // do something with `next.container` for your enter transition
//       // then return a promise or use `this.async()`
//     }
//   }]
// });

/**
 * load any data that your site needs on fist load
 */
Promise.all([
  fetchCart()
]).then(([ cart ]) => {
  /**
   * add the cart data to the picoapp instance
   */
  app.hydrate({ cart })

  console.log('are we waiting on this')

  /**
   * mount any components defined on the page
   */
  app.mount()
})
