export default function options (root, opts = {}, cb = () => {}) {
  if (typeof opts === 'function') {
    cb = opts
    opts = {}
  }

  const state = {}
  const selects = root.querySelectorAll(opts.select || 'select')
  const radios = root.querySelectorAll(opts.radio || `input[type='radio']`)

  for (let i = 0; i < selects.length; i++) {
    state[selects[i].name] = selects[i].value
    selects[i].addEventListener('change', update)
  }

  for (let i = 0; i < radios.length; i++) {
    state[radios[i].name] = state[radios[i].name] || radios[i].value
    radios[i].addEventListener('click', update)
  }

  function update ({ target }) {

    const { name, value } = target
    state[name] = value

    cb(name, value)
  }

  const url = window.location.search

  const urlParams = new URLSearchParams(url)
  const myParam = urlParams.get('variant')

  const meta = JSON.parse(document.querySelector('.js-meta').innerHTML)
  // console.log(meta)

  if (myParam) {

    meta.variants.forEach(function(el) {

      var str = el.title

      if (el.id == myParam) {
        // console.log('Finish')
        // console.log(el)
        // cb('Finish', str)
      }
    })
  }

  return function getState () {
    return state
  }
}
