import getProductJson from '../slater/getProductJson.js'

export default function controller (
  slug = window.location.pathname.split('/').reverse()[0],
  opts = {}
) {
  return getProductJson(slug).then(product => {

    let options = []
    let selectedOptions = {}
    let selectedVariant = null

    function updateSelectedVariant () {
      let res

      outer: for (let i = 0; i < product.variants.length; i++) {
        const variant = product.variants[i]
        const keys = Object.keys(selectedOptions)

        inner: for (let o = 0; o < keys.length; o++) {
          if (variant[`option${o + 1}`] !== selectedOptions[keys[o]]) {
            continue outer
          }
        }

        for (let i = 0; i < options.length; i++) {
          const selected = variant[`option${i + 1}`]

          for (let o = 0; o < options[i].values.length; o++) {
            if (options[i].values[o].value === selected) {
              options[i].values[o].selected = true
            } else {
              options[i].values[o].selected = false
            }
          }
        }

        selectedVariant = variant
        res = variant

        break outer
      }

      return res
    }

    for (let i = 0; i < product.options.length; i++) {
      const { name, values } = product.options[i]

      selectedOptions[name] = values[0]

      options.push({
        name,
        values: values.map(value => ({
          value
        }))
      })
    }

    updateSelectedVariant()

    return {
      get options () {
        return options
      },
      get selected () {
        return selectedVariant
      },
      set (name, value) {
        selectedOptions[name] = value
        updateSelectedVariant()
        return selectedVariant
      }
    }
  })
}
