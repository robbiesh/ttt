const path = require('path')

module.exports = {
  themes: {
    development: {
      id: '79210577994',
      password: 'd58bb78ee6cdb49dca00ab889417484f',
      store: 'tic-tac-toe-demo.myshopify.com',
      ignore: [
        'settings_data.json'
      ]
    }
  }
}
